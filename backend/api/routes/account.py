from fastapi import APIRouter
from fastapi_users import FastAPIUsers
from fastapi_users.jwt import decode_jwt
from fastapi_users.authentication import CookieAuthentication
from fastapi_users.db import SQLAlchemyBaseUserTable, SQLAlchemyUserDatabase
from sqlalchemy.ext.declarative import declarative_base
from databases import Database
from backend.core.config import (SECRET, LIFE_TIME, COOKIE_NAME, SAME_SITE, HTTPONLY, SECURE)
from backend.models.user import *
from backend.db import database as postgres_db
import sqlalchemy as sa

router = APIRouter(prefix="/account", tags=["account"])  # Account router


# Authentication via cookie instance
cookie_authentication = CookieAuthentication(
        secret=str(SECRET),
        lifetime_seconds=int(LIFE_TIME),
        cookie_name=str(COOKIE_NAME),
        cookie_samesite=str(SAME_SITE),
        cookie_httponly=bool(HTTPONLY),
        cookie_secure=bool(SECURE)
    )


def get_fastapi_users(db: Database) -> FastAPIUsers:
    """Creates FastAPIUsers instance"""
    class UserTable(declarative_base(), SQLAlchemyBaseUserTable):
        name = sa.Column("name", sa.String(length=320), nullable=False)
        telegram_alias = sa.Column("telegram_alias", sa.String(length=320), unique=True)
        phone = sa.Column("phone", sa.String(length=320), unique=True, nullable=False)
        address = sa.Column("address", sa.String(length=320), index=True)

    auth_backends = [cookie_authentication]

    users = UserTable.__table__
    user_db = SQLAlchemyUserDatabase(UserDB, db, users)

    fastapi_users = FastAPIUsers(
        user_db,
        auth_backends,
        User,
        UserCreate,
        UserUpdate,
        UserDB,
    )

    return fastapi_users


fastapi_users = get_fastapi_users(postgres_db)  # FastAPIUsers instance with all the configurations

# POST /register
router.include_router(fastapi_users.get_register_router())

# POST /login and POST /logout
router.include_router(fastapi_users.get_auth_router(cookie_authentication))

# POST /forgot-password and POST /reset-password
router.include_router(fastapi_users.get_reset_password_router(str(SECRET)))

# GET and PATCH /me , DELETE and GET and PATCH /{user_id}
router.include_router(fastapi_users.get_users_router())

