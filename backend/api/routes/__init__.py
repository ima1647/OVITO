from fastapi import APIRouter
from backend.api.routes.account import router as account_router
from backend.api.routes.category import router as category_router

router = APIRouter(prefix="/v1", tags=["v1"])  # router /api/v1

router.include_router(account_router)  # Account routes
router.include_router(category_router)  # Category routes
