from fastapi import APIRouter, Body, Depends
from backend.models.category import Category, CategoryCreate
from backend.models.user import User
from starlette.status import HTTP_201_CREATED, HTTP_200_OK
from backend.api.routes.account import fastapi_users
from backend.db.repositories.category import CategoryRepository
from backend.api.dependencies.database import get_repository
from pydantic import UUID4, Field

router = APIRouter(prefix='/category', tags=["categories"])

current_user = fastapi_users.current_user(active=True)


@router.post("/", response_model=Category, status_code=HTTP_201_CREATED)
async def create_category(
        user: User = Depends(current_user),
        new_category: CategoryCreate = Body(...),
        category_repo: CategoryRepository = Depends(get_repository(CategoryRepository))
        ) -> Category:
    """Create new Category"""
    category: Category = Category(**new_category.dict())
    created_category: Category = await category_repo.add_category(category)
    return created_category


@router.get("/by_id/{category_id}", response_model=Category, status_code=HTTP_200_OK)
async def get_category_by_id(
        category_id: UUID4,
        user: User = Depends(current_user),
        category_repo: CategoryRepository = Depends(get_repository(CategoryRepository))
        ) -> Category:
    """Get Category be its ID"""
    category: Category = await category_repo.get_category_by_id(category_id)
    return category


@router.get("/by_name/{category_name}", response_model=Category, status_code=HTTP_200_OK)
async def get_category_by_name(
        category_name: str,
        user: User = Depends(current_user),
        category_repo: CategoryRepository = Depends(get_repository(CategoryRepository))
        ) -> Category:
    """Get Category by its Name"""
    category: Category = await category_repo.get_category_by_name(category_name)
    return category
