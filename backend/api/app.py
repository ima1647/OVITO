from fastapi import FastAPI
from backend.api.routes import router as v1_router
from backend.core.tasks import create_start_app_handler, create_stop_app_handler
from backend.db import database


def get_application():
    """Returns FastAPI application with all the settings"""
    app = FastAPI()
    app.include_router(v1_router, prefix="/api")

    app.add_event_handler("startup", create_start_app_handler(app, database))
    app.add_event_handler("shutdown", create_stop_app_handler(app))

    return app


app = get_application()  # Application
