# Backend

### Workflow
- Create `.env` file
> $ cp ./backend/.env.template ./backend/.env
- Update `.env` file (Optional)
- Start backend
> $ docker-compose -f docker-compose-backend.yml up --build
- Go to `localhost:8000/docs` to see Swagger API Documentation