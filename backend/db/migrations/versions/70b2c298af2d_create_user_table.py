
"""create_user_table

Revision ID: 70b2c298af2d
Revises: 
Create Date: 2022-03-13 16:43:10.208694

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy_utils import UUIDType


# revision identifiers, used by Alembic
revision = '70b2c298af2d'
down_revision = None
branch_labels = None
depends_on = None


def create_user_table() -> None:
    op.create_table(
        "user",
        sa.Column("id", UUIDType(binary=False), primary_key=True),
        sa.Column("email", sa.String(length=320), unique=True, index=True, nullable=False),
        sa.Column("hashed_password", sa.String(length=320), nullable=False),
        sa.Column("is_active", sa.Boolean, default=True, nullable=False),
        sa.Column("is_superuser", sa.Boolean, default=False, nullable=False),
        sa.Column("is_verified", sa.Boolean, default=False, nullable=False),
        sa.Column("name", sa.String(length=320), nullable=False),
        sa.Column("telegram_alias", sa.String(length=320), unique=True),
        sa.Column("phone", sa.String(length=320), unique=True, nullable=False),
        sa.Column("address", sa.String(length=320), index=True)
    )


def upgrade() -> None:
    create_user_table()


def downgrade() -> None:
    op.drop_table("user")
