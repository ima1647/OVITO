
"""category

Revision ID: 86655b6ed730
Revises: 70b2c298af2d
Create Date: 2022-03-13 21:00:31.473555

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy_utils import UUIDType


# revision identifiers, used by Alembic
revision = '86655b6ed730'
down_revision = '70b2c298af2d'
branch_labels = None
depends_on = None


def create_category_table():
    op.create_table(
        "category",
        sa.Column("id", UUIDType(binary=False), primary_key=True),
        sa.Column("name", sa.String(length=320), unique=True, index=True, nullable=True)
    )


def upgrade() -> None:
    create_category_table()


def downgrade() -> None:
    op.drop_table("category")
