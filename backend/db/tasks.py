from fastapi import FastAPI
from databases import Database
import logging


async def connect_to_db(app: FastAPI, database: Database):
    """Connects to database"""
    try:
        await database.connect()
        app.state.db = database
        logging.info("Database Connection Success")
    except Exception as e:
        logging.warning("Database Connection Error")
        logging.warning(str(e))


async def disconnect_from_db(app: FastAPI):
    await app.state.db.disconnect()
    logging.info("Database Disconnected")
