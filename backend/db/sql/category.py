CREATE_CATEGORY_QUERY = """
    INSERT INTO category
    VALUES (:id, :name)
    RETURNING id, name
"""

GET_CATEGORY_BY_ID_QUERY = """
    SELECT * FROM category
    WHERE id = :category_id
"""

GET_CATEGORY_BY_NAME_QUERY = """
    SELECT * FROM category
    WHERE name = :category_name
"""