from backend.db.tasks import connect_to_db, disconnect_from_db
from fastapi import FastAPI
from databases import Database
from typing import Callable


def create_start_app_handler(app: FastAPI, database: Database) -> Callable:
    """Creates start application handler"""
    async def start_app():
        await connect_to_db(app, database)

    return start_app


def create_stop_app_handler(app: FastAPI) -> Callable:
    """Create shut down application handler"""
    async def shut_down():
        await disconnect_from_db(app)

    return shut_down
