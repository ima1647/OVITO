# OVITO: there is almost everything (maybe)

## How to run locally
```bash
docker-compose up
```

**Warning:** for the development, serve locally (front-end)

## Features

0. Base of the app, product creation form, profile page, cards for products [Alina]
1. Product editing, searching elements insertion, categories form [Natasha]
2. Authorization and personal accounts [Nikita]
4. Integration with Google Maps _+ location detection_ [Marita]
5. Placing an advertisement using Telegram bot [Ruslan]
6. Search for ads within a given radius [Ruslan + Marina]

[//]: # (6. Automatized sending of the advertisements to different telegram chats according to the item category [Niki])

## Todo (global), required for the mark

- [ ] Add backend (api)
- [ ] Add CI
  - [ ] Static analysis
  - [ ] Unit testing, Integration testing, UI testing (Coverage from 60% statement)
  - [ ] Mutation testing, Fuzz testing, Stress testing
  - [ ] Recovery implementation
  - [ ] UI testing
  - [ ] Dashboard and service monitoring
- [ ] Add CD

## References

- [Design](https://www.figma.com/file/BTfablSBji62eVrUn56b8k/Ovito?node-id=0%3A1)
