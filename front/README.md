# OVITO-Frontend

## Sub-Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Todo (front)

- [x] put header to the root component
- [x] create product page (according to the design)
- [x] separate frontend from backend in the repo
- [x] create a profile page
  - [x] image selector
  - [x] image preview
  - [x] edition form (not creation)
- [x] create search mechanism (apply the existing components)
- [ ] add api
- [x] category editing
- [x] docker for frontend
- [ ] jest (logic found in: Select component, Form component)
- [x] ci frontend
