import { createStore } from 'vuex';

export default createStore({
  state () {
    return {
      userId: 0,
      //temporary, while there is no backend
      products: [
        {
          id: 1,
          title: 'Michelin used winter tires',
          description: 'Studded tires\n' +
              'Michelin 97H X-Ice North 225/55/16.\n' +
              '225 55 r16, 2255516, r16 225 55, р16 225 55, 225 55 р16, р16 22555, 22555 r16\n' +
              'Kazan\n' +
              '5 hours ago',
          location: 'Kazan',
          time: '5 h',
          imgs: ['https://s3-alpha-sig.figma.com/img/5ef1/6476/b2b852ebc3c1add473b174e3b398c9c9?Expires=1647820800&Signature=I429hdJVOAv1tiiPsIj68EDL~1VgH1m1eZaDpCsZRscLndyz5hDPqwCt7CXJYa3pTwuXD5Rt6TVmFx04L2PhXwIQHa2L4rD3wKLCIYcKhxS6d5chScM-tGu3lutStLEqC4XIGqIkqNHS9TsgJObh5otLaSKyDLnDA4sGmwibKAqT5a6Lch6NqGLluui~R-h~lEp~HIGSxp-RiB2i284NCzyoUXJc17tPzaD-vy2NRPVY7dT26PNc4JcShMIIMcW6rg87jj5rgLe7zGH4ugRm41-KAkabnX~nA9OdjihZehpOwe2SAufx~BWU2-7uSAte7RMF~j5tzIZuJPVWASvrMw__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA'],
          price: 5000,
          user: {
            username: 'Maxim',
            address: 'Innopolis, Universitetskaya 1-4-305',
            alias: 'nkt929',
            phone: '89537646722'
          }
        },
        {
          id: 2,
          title: 'Nokian winter tires new',
          description: 'Studded tires 205 60 16 Nokian Hakkapeliitta 8  205 60 R16 108Z',
          location: 'Kazan',
          time: '10 h',
          imgs: ['https://s3-alpha-sig.figma.com/img/fd7c/1a8a/5fb9dcd841d3fc22ca43fbc9566326ec?Expires=1647820800&Signature=YgkphNgdBeT62sG1qO~6ghgQVe2EFZcTEcuZcYNCrYvtxIsjr7CE0zlJ0aSQDcyjps1a0hg3jY9iAYbM4l06~MZdd6-BoMADndQvxq70PLG4OS53hQhqkB9wRUT7PTJMM-wWoebeUgkn~taz9C~6RHsT-p1~enPmhsgcXfkGs21MlfhJIPrud32Gp0R9ve9EVZVdMuCvqyMn0pUzZdfPp7ID7Rx727IlW6QTNV1PO0gnEnuoIiBH8dW3KLSpYqLSM3bdaO-yPmqEWF5g-85uL1ePwXVO-i12Sy8oWcvwmhvXhxyabpbbGkJ5aIzBzBF7IR5psYOp3MZ2-QSTe4BD1Q__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA'],
          price: 3500,
          user: {
            username: 'Maxim',
            address: 'Innopolis, Universitetskaya 1-4-305',
            alias: 'MefAldemisov',
            phone: '89537646722'
          }
        }
      ],
      cats: [
        { id: 0, label: 'car' },
        { id: 1, label: 'personal computers' },
        { id: 2, label: 'phones' },
        { id: 3, label: 'books' },
        { id: 4, label: 'trains' },
        { id: 5, label: 'kitchen' },
        { id: 6, label: 'other' },
      ]
    }
  },
});
