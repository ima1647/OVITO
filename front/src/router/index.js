import { createRouter, createWebHashHistory } from 'vue-router';

export default createRouter({
  history: createWebHashHistory(),
  routes: [
    { path: '/', name: 'search', component: () => import('../views/SearchPage') },
    {
      path: '/user/:id',
      name: 'user',
      component:  () => import('../views/UserPage'),
      children: [
        {
          path: 'list',
          name: 'user-edit',
          component: () => import('../views/user/ListProducts')
        },
        {
          path: 'create',
          name: 'user-create',
          component: () => import('../views/user/ProductForm'),
        },
        {
          path: 'edit/:productId',
          name: 'user-product',
          component: () => import('../views/user/ProductForm'),
        },
        {
          path: 'favourites',
          name: 'user-liked',
          component: () => import('../views/user/FavouriteProducts')
        },
        {
          path: 'categories',
          name: 'user-cats',
          component: () => import('../views/user/EditCats')
        }

      ]
    },
    { path: '/product/:id', name: 'product', component:  () => import('../views/ProductPage') },
  ]
});